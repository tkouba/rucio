Rucio Package
=============


Development takes place via the usual Rucio processes as outlined in `<http://rucio.cern.ch/>`_.

The master repository is on `<http://git.cern.ch/pubweb/rucio.git/tree>`_.

See release notes and more at `<https://its.cern.ch/jira/browse/RUCIO>`_.